﻿using ProjectStructure.Shared.DTO.Team;
using ProjectStructure.Shared.DTO.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectStructure.Shared.DTO.Project
{
    public class ProjectDTO
    {
        public int Id { get; set; }
        public UserDTO Author { get; set; }
        public TeamDTO Team { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime? Deadline { get; set; }
        public DateTime? CreatedAt { get; set; }
    }
}
