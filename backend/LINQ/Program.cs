﻿using AutoMapper;
using LINQ.Models;
using LINQ.Services;
using ProjectStructure.Shared.DTO.Task;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MapperConfiguration = LINQ.Models.MapperConfiguration;

namespace LINQ
{
    class Program
    {
        static async System.Threading.Tasks.Task Main(string[] args) // Я правильно зрозумів, що Мейн треба було зробити асинк, аби позбутися .Result всередині нього?
        {

            var queries = new TaskService();

            var markedTaskId = await queries.MarkRandomTaskWithDelay(1000);

            Console.WriteLine(markedTaskId);

            Console.ReadKey();
        }
    }
}
