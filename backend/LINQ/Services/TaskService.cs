﻿using AutoMapper;
using LINQ.Models;
using ProjectStructure.Shared.DTO.Task;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using MapperConfiguration = LINQ.Models.MapperConfiguration;

namespace LINQ.Services
{
    public class TaskService
    {
        static readonly Random random = new Random();

        Timer timer = new Timer();
        HTTPService hTTPService;

        private MapperConfiguration config = new MapperConfiguration();
        IMapper mapper;

        public TaskService()
        {
            timer.AutoReset = false;
            hTTPService = new HTTPService();
            mapper = new Mapper(config.Configure());
        }
        
        public async Task<int> MarkRandomTaskWithDelay(int delay)
        {
            timer.Interval = delay;

            var tcs = new TaskCompletionSource<int>();

            ElapsedEventHandler taskFive = async (o, e) =>
            {
                var tasks = await hTTPService.GetTasks(); // знаходжу список тасків
                var unfinishedTasks = tasks.Where(x => x.FinishedAt == null); // перевіряю які ще не завершені
                int randInt = random.Next(unfinishedTasks.Count() - 1); // вибираю рандомно один з них
                Models.Task finishedTask = unfinishedTasks.ElementAt(randInt);
                finishedTask.State = Models.Enums.State.Finished;
                finishedTask.FinishedAt = DateTime.Now;

                var taskDTO = mapper.Map<Models.Task, TaskDTO>(finishedTask); // маплю в ДТО, на клієнті ж треба мапити об'єкти, чи ні?
                var result = await hTTPService.UpdateTask(taskDTO, taskDTO.Id);

                if (result == null)
                {
                    throw new Exception();
                }

                tcs.SetResult(result.Id);
            };

            timer.Elapsed += taskFive;
            timer.Start();            

            return await tcs.Task;
        }

    }
}
