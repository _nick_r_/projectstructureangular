﻿using LINQ.Models;
using ProjectStructure.Shared.DTO.Project;
using ProjectStructure.Shared.DTO.Task;
using ProjectStructure.Shared.DTO.Team;
using ProjectStructure.Shared.DTO.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace LINQ.Services
{
    class HTTPService : IDisposable    // Питання, цей клас краще розділити на чотири класи, аби один відповідав за Tasks, інший за юзерс і тд?
    {
        private HttpClient Client { get; set; } = new HttpClient();

        private string BaseURL = Settings.baseURL;

        public HTTPService()
        {

        }

        public async Task<Models.Task[]> GetTasks()
        {
            var response = await Client.GetAsync(BaseURL + "Tasks");           
            return JsonSerializer.Deserialize<Models.Task[]>(await response.Content.ReadAsStringAsync());
        }

        public async Task<Models.Task> GetTask(int id)
        {
            var response = await Client.GetAsync(BaseURL + "Tasks/" + id);
            return JsonSerializer.Deserialize<Models.Task>(await response.Content.ReadAsStringAsync());
        }

        public async Task<Models.Task> UpdateTask(TaskDTO taskDTO, int id)
        {
            var task = JsonSerializer.Serialize(taskDTO);

            var requestContent = new StringContent(task, Encoding.UTF8, "application/json");

            var response = await Client.PutAsync(BaseURL + "Tasks/" + id, requestContent);
            return JsonSerializer.Deserialize<Models.Task>(await response.Content.ReadAsStringAsync());
        }

        public async Task<Models.Task> CreateTask(TaskDTO taskDTO)
        {
            var task = JsonSerializer.Serialize(taskDTO);

            var requestContent = new StringContent(task, Encoding.UTF8, "application/json");

            var response = await Client.PostAsync(BaseURL + "Tasks", requestContent);
            return JsonSerializer.Deserialize<Models.Task>(await response.Content.ReadAsStringAsync());
        }

        public async Task<Team[]> GetTeams()
        {
            var response = await Client.GetAsync(BaseURL + "Teams");
            return JsonSerializer.Deserialize<Team[]>(await response.Content.ReadAsStringAsync());
        }

        public async Task<Team> GetTeam(int id)
        {
            var response = await Client.GetAsync(BaseURL + "Teams/" + id);
            return JsonSerializer.Deserialize<Team>(await response.Content.ReadAsStringAsync());
        }

        public async Task<Team> UpdateTeam(TeamDTO teamDTO, int id)
        {
            var team = JsonSerializer.Serialize(teamDTO);

            var requestContent = new StringContent(team, Encoding.UTF8, "application/json");

            var response = await Client.PutAsync(BaseURL + "Teams/" + id, requestContent);
            return JsonSerializer.Deserialize<Models.Team>(await response.Content.ReadAsStringAsync());
        }

        public async Task<Team> CreateTeam(TeamDTO teamDTO)
        {
            var team = JsonSerializer.Serialize(teamDTO);

            var requestContent = new StringContent(team, Encoding.UTF8, "application/json");

            var response = await Client.PostAsync(BaseURL + "Teams", requestContent);
            return JsonSerializer.Deserialize<Team>(await response.Content.ReadAsStringAsync());
        }

        public async Task<User[]> GetUsers()
        {
            var response = await Client.GetAsync(BaseURL + "Users");
            return JsonSerializer.Deserialize<User[]>(await response.Content.ReadAsStringAsync());
        }

        public async Task<User> GetUser(int id)
        {
            var response = await Client.GetAsync(BaseURL + "Users/" + id);
            return JsonSerializer.Deserialize<User>(await response.Content.ReadAsStringAsync());
        }

        public async Task<User> UpdateUser(UserDTO userDTO, int id)
        {
            var user = JsonSerializer.Serialize(userDTO);

            var requestContent = new StringContent(user, Encoding.UTF8, "application/json");

            var response = await Client.PutAsync(BaseURL + "Users/" + id, requestContent);
            return JsonSerializer.Deserialize<User>(await response.Content.ReadAsStringAsync());
        }

        public async Task<User> CreateUser(UserDTO userDTO)
        {
            var user = JsonSerializer.Serialize(userDTO);

            var requestContent = new StringContent(user, Encoding.UTF8, "application/json");

            var response = await Client.PostAsync(BaseURL + "Users", requestContent);
            return JsonSerializer.Deserialize<User>(await response.Content.ReadAsStringAsync());
        }

        public async Task<Project[]> GetProjects()
        {
            var response = await Client.GetAsync(BaseURL + "Projects");
            return JsonSerializer.Deserialize<Project[]>(await response.Content.ReadAsStringAsync());
        }

        public async Task<Project> GetProject(int id)
        {
            var response = await Client.GetAsync(BaseURL + "Projects/" + id);
            return JsonSerializer.Deserialize<Project>(await response.Content.ReadAsStringAsync());
        }

        public async Task<Project> UpdateProject(ProjectDTO projectDTO, int id)
        {
            var project = JsonSerializer.Serialize(projectDTO);

            var requestContent = new StringContent(project, Encoding.UTF8, "application/json");

            var response = await Client.PutAsync(BaseURL + "Projects/" + id, requestContent);
            return JsonSerializer.Deserialize<Project>(await response.Content.ReadAsStringAsync());
        }

        public async Task<Project> CreateProjec(ProjectDTO projectDTO)
        {
            var project = JsonSerializer.Serialize(projectDTO);

            var requestContent = new StringContent(project, Encoding.UTF8, "application/json");

            var response = await Client.PostAsync(BaseURL + "Projects", requestContent);
            return JsonSerializer.Deserialize<Project>(await response.Content.ReadAsStringAsync());
        }

        public void Dispose()
        {
            Client.Dispose();
        }
    }
}
