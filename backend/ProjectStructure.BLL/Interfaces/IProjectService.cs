﻿using ProjectStructure.Shared.DTO.Project;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.Interfaces
{
    public interface IProjectService
    {
        Task<ProjectDTO> AddProject(ProjectCreateDTO aircraft);
        Task<ICollection<ProjectDTO>> GetAllProjects();
        Task<ProjectDTO> GetProject(int id);
        Task<ProjectDTO> UpdateProject(int id, ProjectDTO aircraft);
        Task DeleteProject(int id);
    }
}
