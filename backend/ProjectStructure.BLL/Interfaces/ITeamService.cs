﻿using ProjectStructure.Shared.DTO.Team;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.Interfaces
{
    public interface ITeamService
    {
        Task<TeamDTO> AddTeam(TeamDTO aircraft);
        Task<List<TeamDTO>> GetAllTeams();
        Task<TeamDTO> GetTeam(int id);
        Task<TeamDTO> UpdateTeam(int id, TeamDTO aircraft);
        Task DeleteTeam(int id);
    }
}
