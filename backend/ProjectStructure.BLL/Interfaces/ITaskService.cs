﻿using ProjectStructure.Shared.DTO.Task;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.Interfaces
{
    public interface ITaskService
    {
        Task<TaskDTO> AddTask(TaskDTO aircraft);
        Task<List<TaskDTO>> GetAllTasks();
        Task<TaskDTO> GetTask(int id);
        Task<TaskDTO> UpdateTask(int id, TaskDTO aircraft);
        Task DeleteTask(int id);
    }
}
