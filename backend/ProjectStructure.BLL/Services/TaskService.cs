﻿using AutoMapper;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.DataAccess.Interfaces;
using ProjectStructure.Shared.DTO.Task;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.Services
{
    public class TaskService : ITaskService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public TaskService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        public async Task<TaskDTO> AddTask(TaskDTO task)
        {
            DataAccess.Models.Task taskModel = mapper.Map<TaskDTO, DataAccess.Models.Task>(task);
            taskModel.CreatedAt = DateTime.Now;
            DataAccess.Models.Task result = await unitOfWork.Tasks.Create(taskModel);
            await unitOfWork.SaveChangesAsync();
            return mapper.Map<DataAccess.Models.Task, TaskDTO>(result);
        }

        public async Task DeleteTask(int id)
        {
            try
            {
                await unitOfWork.Tasks.Delete(id);
                await unitOfWork.SaveChangesAsync();
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public async Task<List<TaskDTO>> GetAllTasks()
        {
            IEnumerable<DataAccess.Models.Task> task = await unitOfWork.Tasks.GetList();
            return mapper.Map<IEnumerable<DataAccess.Models.Task>, List<TaskDTO>>(task);
        }

        public async Task<TaskDTO> GetTask(int id)
        {
            DataAccess.Models.Task task = await unitOfWork.Tasks.Get(id);
            return mapper.Map<DataAccess.Models.Task, TaskDTO>(task);
        }

        public async Task<TaskDTO> UpdateTask(int id, TaskDTO task)
        {
            try
            {
                DataAccess.Models.Task taskModel = mapper.Map<TaskDTO, DataAccess.Models.Task>(task);
                DataAccess.Models.Task result = await unitOfWork.Tasks.Update(id, taskModel);
                await unitOfWork.SaveChangesAsync();
                return mapper.Map<DataAccess.Models.Task, TaskDTO>(result);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }
    }
}
