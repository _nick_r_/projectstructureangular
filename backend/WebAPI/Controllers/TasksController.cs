﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.Shared.DTO.Task;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly ITaskService taskService;

        public TasksController(ITaskService taskService)
        {
            this.taskService = taskService;
        }

        [HttpGet]
        public async Task<ActionResult<ICollection<TaskDTO>>> GetTasks()
        {
            return Ok(await taskService.GetAllTasks());
        }

        [HttpPost]
        public async Task<ActionResult<TaskDTO>> CreateTask([FromBody] TaskDTO newTask)
        {
            return Ok(await taskService.AddTask(newTask));
        }

        [HttpPut("{id}")]
        public async Task<ActionResult<TaskDTO>> UpdateTask(int id, [FromBody] TaskDTO newTask)
        {
            return Ok(await taskService.UpdateTask(id, newTask));
        }

        [HttpDelete("{id:int}")]
        public async Task<IActionResult> DeleteTask(int id)
        {
            await taskService.DeleteTask(id);
            return NoContent();
        }

        [HttpGet("{id:int}")]
        public async Task<ActionResult<TaskDTO>> GetTask(int id)
        {
            return Ok(await taskService.GetTask(id));

        }
    }
}
