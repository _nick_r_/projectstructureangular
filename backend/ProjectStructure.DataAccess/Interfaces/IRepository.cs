﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectStructure.DataAccess.Interfaces
{
    public interface IRepository<T>
        where T : class
    {
        Task<ICollection<T>> GetList(); // получение всех объектов
        Task<T> Get(int id); // получение одного объекта по id
        Task<T> Create(T item); // создание объекта
        Task<T> Update(int id, T item); // обновление объекта
        Task Delete(int id); // удаление объекта по id

    }
}
