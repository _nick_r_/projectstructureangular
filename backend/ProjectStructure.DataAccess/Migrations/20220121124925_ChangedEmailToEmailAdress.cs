﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ProjectStructure.DataAccess.Migrations
{
    public partial class ChangedEmailToEmailAdress : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Email",
                table: "Users",
                newName: "EmailAdress");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "EmailAdress",
                table: "Users",
                newName: "Email");
        }
    }
}
