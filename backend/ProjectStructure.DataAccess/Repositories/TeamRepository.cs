﻿using Microsoft.EntityFrameworkCore;
using ProjectStructure.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace ProjectStructure.DataAccess.Interfaces.Repositories
{
    public class TeamRepository : IRepository<Team>
    {
        protected readonly DataContext context;

        public TeamRepository(DataContext context)
        {
            this.context = context;
        }

        public async Task<Team> Create(Team item)
        {
            await context.Teams.AddAsync(item);
            return item;
        }

        public async System.Threading.Tasks.Task Delete(int id)
        {
            Team team = await Get(id);
            if (team == null)
                throw new System.Exception("Invalid id");

            context.Teams.Remove(team);
        }

        public async Task<Team> Get(int id)
        {
            return await context.Teams.FirstOrDefaultAsync(c => c.Id == id);
        }

        public async Task<ICollection<Team>> GetList()
        {
            return await context.Teams.ToListAsync();
        }

        public async Task<Team> Update(int id, Team item)
        {
            Team team = await Get(id);
            if (team == null)
                throw new System.Exception("Invalid id");
            team.Name = item.Name;
            team.CreatedAt = team.CreatedAt;
            return team;
        }
    }
}
