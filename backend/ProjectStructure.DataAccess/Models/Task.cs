﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using ProjectStructure.DataAccess.Models.Abstract;
using ProjectStructure.DataAccess.Models.Enums;

namespace ProjectStructure.DataAccess.Models
{
    public class Task : BaseEntity
    {
        [Required]
        public int ProjectId { get; set; }
        public Project Project { get; set; }

        
        public int? PerformerId { get; set; }
        public User Performer { get; set; }

        [MinLength(3), MaxLength(40)]
        public string Name { get; set; }

        [MaxLength(200)]
        public string Description { get; set; }
        public State State { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? FinishedAt { get; set; }
    }
}
