﻿using ProjectStructure.DataAccess.Models.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectStructure.DataAccess.Models
{
    public class Project : BaseEntity
    {
        [Required]
        public int AuthorId { get; set; }
        public User Author { get; set; }

        [Required]
        public int TeamId { get; set; }
        public Team Team { get; set; }

        [MinLength(2), MaxLength(40)]
        public string Name { get; set; }

        [MaxLength(200)]
        public string Description { get; set; }
        public DateTime? Deadline { get; set; }
        public DateTime? CreatedAt { get; set; }

        public ICollection<Task> Tasks { get; set; }
    }
}
