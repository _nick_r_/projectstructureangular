﻿using ProjectStructure.DataAccess.Models.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectStructure.DataAccess.Models
{
    public class User : BaseEntity
    {
        
        public int? TeamId { get; set; }
        public Team Team { get; set; }

        [MinLength(2), MaxLength(15)]
        public string FirstName { get; set; }

        [MinLength(2), MaxLength(30)]
        public string LastName { get; set; }

        [MinLength(6), MaxLength(30)]
        public string Email { get; set; }
        public DateTime RegisteredAt { get; set; }
        public DateTime? BirthDay { get; set; }

        public ICollection<Models.Task> Tasks { get; set; }
        public ICollection<Models.Project> Projects { get; set; }

    }
}
