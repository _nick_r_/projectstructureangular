import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProjectsComponent } from './components/projects/projects.component';
import { TasksComponent } from './components/tasks/tasks.component';
import { TeamsComponent } from './components/teams/teams.component';
import { UsersComponent } from './components/users/users.component';
import { ExitGuard } from './guards/exit.guard';

const routes: Routes = [
  { path: '', component: ProjectsComponent, pathMatch: 'full' },
  { path: 'Projects', component: ProjectsComponent, pathMatch: 'full' },
  { path: 'Tasks', component: TasksComponent, pathMatch: 'full', canDeactivate: [ExitGuard] },
  { path: 'Teams', component: TeamsComponent, pathMatch: 'full' },
  { path: 'Users', component: UsersComponent, pathMatch: 'full' },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [ExitGuard],
})
export class AppRoutingModule { }
