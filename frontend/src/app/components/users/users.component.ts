import { Component, OnInit } from '@angular/core';
import { Subject, takeUntil } from 'rxjs';
import { Team } from 'src/app/models/team/team';
import { NewUser } from 'src/app/models/user/newUser';
import { User } from 'src/app/models/user/user';
import { TeamService } from 'src/app/services/team.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  public users: User[] = [];
  public cachedUsers: User[] = [];
  public teams: Team[] = [];
  public cachedTeams: Team[] = [];
  public newTeamName : string;
  public user = {} as NewUser;

  public loadingUsers = false;
  public loading = false;
  

  private unsubscribe$ = new Subject<void>();

  constructor(
    private userService: UserService,
    private teamService: TeamService
  ) { }

  ngOnInit(): void {
    this.getUsers();
    this.getTeams();
  }

  public getUsers() {
    this.loadingUsers = true;
    this.userService
      .getUsers()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (resp) => {
          this.loadingUsers = false;
          this.users = this.cachedUsers = resp.body;
        },
        (error) => (this.loadingUsers = false)
      );
  }

  public getTeams() {
    this.teamService
      .getTeams()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (resp) => {
          this.teams = this.cachedTeams = resp.body;
        },
      );
  }

  public sendUser(){

    let team = this.teams.find(x => x.name === this.newTeamName);
    this.user.teamId = team.id;
    const projectSubscription = this.userService.createUser(this.user);
    this.loading = true;

    projectSubscription.pipe(takeUntil(this.unsubscribe$)).subscribe(
      (respUser) => {
          this.addNewProject(respUser.body);
          this.user.firstName = undefined;
          this.user.lastName = undefined;
          this.user.birthDay = undefined;
          this.user.email = undefined;
          this.loading = false;
      }
    );
  }

  public addNewProject(newProject: User) {
    if (!this.cachedUsers.some((x) => x.id === newProject.id)) {
        this.cachedUsers = this.sortProjectArray(this.cachedUsers.concat(newProject));
    }    
  }

  private sortProjectArray(array: User[]): User[] {
    return array.sort((a, b) => +new Date(b.registeredAt) - +new Date(a.registeredAt));
  }

}
