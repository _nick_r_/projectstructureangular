import { Component, Input, OnInit } from '@angular/core';
import { Subject, takeUntil } from 'rxjs';
import { Team } from 'src/app/models/team/team';
import { TeamService } from 'src/app/services/team.service';

@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.css']
})
export class TeamComponent implements OnInit {

  @Input() public team: Team;

  public showEditContainer = false;

  private unsubscribe$ = new Subject<void>();

  constructor(
    private teamService: TeamService
  ) { }

  ngOnInit(): void {
  }


  public toggleEditContainer() {
    this.showEditContainer = !this.showEditContainer;
  }

  public deleteTeam() {
    this.teamService
      .deleteTeam(this.team.id).pipe(takeUntil(this.unsubscribe$))
      .subscribe(() => {
        window.location.reload();

      });
  }

  public editTeam() {
    this.showEditContainer = !this.showEditContainer;
    const teamSubscription = this.teamService.editTeam(this.team.id, this.team)


    teamSubscription.pipe(takeUntil(this.unsubscribe$)).subscribe(
      (resp) => {
        window.location.reload();
      }
    );
  }
}
