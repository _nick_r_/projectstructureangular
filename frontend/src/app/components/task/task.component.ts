import { Component, Input, OnInit } from '@angular/core';
import { Subject, takeUntil } from 'rxjs';
import { Task } from 'src/app/models/task/task';
import { TaskService } from 'src/app/services/task.service';


@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']
})
export class TaskComponent implements OnInit {

  @Input() public task: Task;

  public showEditContainer = false;
  private unsubscribe$ = new Subject<void>();

  constructor(
    private taskService: TaskService
  ) { }

  ngOnInit(): void {
  }

  public deleteTask() {
    this.taskService
      .deleteTask(this.task.id).pipe(takeUntil(this.unsubscribe$))
      .subscribe(() => {
        window.location.reload();

      });
  }
  public toggleEditContainer() {
    this.showEditContainer = !this.showEditContainer;
  }

  public editTask() {
    this.showEditContainer = !this.showEditContainer;
    const taskSubscription = this.taskService.editTask(this.task.id, this.task)


    taskSubscription.pipe(takeUntil(this.unsubscribe$)).subscribe(
      (resp) => {
        window.location.reload();
      }
    );
  }
}
