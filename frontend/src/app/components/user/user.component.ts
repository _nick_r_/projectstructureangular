import { Component, Input, OnInit } from '@angular/core';
import { Subject, takeUntil } from 'rxjs';
import { User } from 'src/app/models/user/user';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  @Input() public user: User;

  public showEditContainer = false;
  private unsubscribe$ = new Subject<void>();

  constructor(
    private userService : UserService
  ) { }

  ngOnInit(): void {
  }


  public deleteUser() {
    this.userService
      .deleteUser(this.user.id).pipe(takeUntil(this.unsubscribe$))
      .subscribe(() => {
        window.location.reload();

      });
  }
  public toggleEditContainer() {
    this.showEditContainer = !this.showEditContainer;
  }

  public editUser() {
    this.showEditContainer = !this.showEditContainer;
    const userSubscription = this.userService.editUser(this.user.id, this.user)


    userSubscription.pipe(takeUntil(this.unsubscribe$)).subscribe(
      (resp) => {
        window.location.reload();
      }
    );
  }
}
