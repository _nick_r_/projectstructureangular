import { Component, Input, OnInit } from '@angular/core';
import { Subject, takeUntil } from 'rxjs';
import { Project } from 'src/app/models/project/project';
import { ProjectService } from 'src/app/services/project.service';

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.css']
})
export class ProjectComponent implements OnInit {

  @Input() public project: Project;

  public showEditContainer = false;

  private unsubscribe$ = new Subject<void>();

  constructor(
    private projectService : ProjectService
  ) { }


  ngOnInit(): void {
  }

  public toggleEditContainer() {
    this.showEditContainer = !this.showEditContainer;
  }

  public deleteProject() {
    this.projectService
      .deleteProject(this.project.id).pipe(takeUntil(this.unsubscribe$))
      .subscribe(() => {
        window.location.reload();

      });
  }

  public editProject() {
    this.showEditContainer = !this.showEditContainer;
    const teamSubscription = this.projectService.editProject(this.project.id, this.project)


    teamSubscription.pipe(takeUntil(this.unsubscribe$)).subscribe(
      (resp) => {
        window.location.reload();
      }
    );
  }
}
