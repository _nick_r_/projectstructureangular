import { Component, OnInit } from '@angular/core';
import { Subject, takeUntil } from 'rxjs';
import { NewProject } from 'src/app/models/project/newProject';
import { Project } from 'src/app/models/project/project';
import { Team } from 'src/app/models/team/team';
import { ProjectService } from 'src/app/services/project.service';
import { TeamService } from 'src/app/services/team.service';
import { State } from 'src/app/models/state/state';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css']
})
export class ProjectsComponent implements OnInit {

  public projects: Project[] = [];
  public cachedProjects: Project[] = [];
  public teams: Team[] = [];
  public cachedTeams: Team[] = [];
  public project = {} as NewProject;
  public newTeamName : string;

  private unsubscribe$ = new Subject<void>();
  public loadingProjects = false;
  public loading = false;

  public constructor(
    private projectService: ProjectService,
    private teamService: TeamService
  ) { }

  ngOnInit(): void {
    this.getProjects();
    this.getTeams();

  }

  public getProjects() {
    this.loadingProjects = true;
    this.projectService
      .getProjects()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (resp) => {
          this.loadingProjects = false;
          this.projects = this.cachedProjects = resp.body;
        },
        (error) => (this.loadingProjects = false)
      );
  }

  public getTeams() {
    this.teamService
      .getTeams()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (resp) => {
          this.teams = this.cachedTeams = resp.body;
        },
      );
  }

  public sendProjects(){

    let team = this.teams.find(x => x.name === this.newTeamName);
    this.project.teamId = team.id;
    const projectSubscription = this.projectService.createProject(this.project);
    this.loading = true;

    projectSubscription.pipe(takeUntil(this.unsubscribe$)).subscribe(
      (respProject) => {
          this.addNewProject(respProject.body);
          this.project.name = undefined;
          this.project.description = undefined;
          this.project.deadline = undefined;
          this.loading = false;
      }
    );
  }

  public addNewProject(newProject: Project) {
    if (!this.cachedProjects.some((x) => x.id === newProject.id)) {
        this.cachedProjects = this.sortProjectArray(this.cachedProjects.concat(newProject));
    }    
  }

  private sortProjectArray(array: Project[]): Project[] {
    return array.sort((a, b) => +new Date(b.createdAt) - +new Date(a.createdAt));
  }
}
