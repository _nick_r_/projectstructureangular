import { Component, OnInit } from '@angular/core';
import { Observable, Subject, takeUntil } from 'rxjs';
import { TaskService } from 'src/app/services/task.service';
import { UserService } from 'src/app/services/user.service';
import { Task } from 'src/app/models/task/task';
import { Project } from 'src/app/models/project/project';
import { ProjectService } from 'src/app/services/project.service';
import { User } from 'src/app/models/user/user';
import { State } from 'src/app/models/state/state';
import { newTask } from 'src/app/models/task/newTask';
import { ComponentCanDeactivate } from 'src/app/guards/exit.guard';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})
export class TasksComponent implements OnInit, ComponentCanDeactivate {

  public tasks: Task[] = [];
  public cachedTasks: Task[] = [];
  public projects: Project[] = [];
  public cachedProjects: Project[] = [];
  public users: User[] = [];
  public cachedUsers: User[] = [];

  public task = {} as newTask;

  public newProjectName: string;
  public newPerformerId: number;
  public newTaskState: State;
  public state = State;

  selected: State = State.Started;
  states = [State.Started, State.InProgress, State.Testing, State.Finished];

  public loadingTasks = false;
  public loading = false;

  private unsubscribe$ = new Subject<void>();

  constructor(
    private taskService: TaskService,
    private projectService: ProjectService,
    private userService: UserService

  ) { }

  ngOnInit(): void {
    this.getTasks();
    this.getProjects();
    this.getUsers();
  }

  public getTasks() {
    this.loadingTasks = true;
    this.taskService
      .getTasks()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (resp) => {
          this.loadingTasks = false;
          this.tasks = this.cachedTasks = resp.body;
        },
        (error) => (this.loadingTasks = false)
      );
  }

  public getProjects() {
    this.projectService
      .getProjects()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (resp) => {
          this.projects = this.cachedProjects = resp.body;
        },
      );
  }

  public getUsers() {
    this.userService
      .getUsers()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (resp) => {
          this.users = this.cachedUsers = resp.body;
        },
      );
  }

  public sendTask() {

    let project = this.projects.find(x => x.name === this.newProjectName);
    this.task.projectId = project.id;
    this.task.performerId = this.newPerformerId;
    console.log(this.newPerformerId);
    const taskSubscription = this.taskService.createTask(this.task);
    this.loading = true;

    taskSubscription.pipe(takeUntil(this.unsubscribe$)).subscribe(
      (respProject) => {
        //this.addNewProject(respProject.body);
        this.task.name = undefined;
        this.task.description = undefined;
        this.task.performerId = undefined;
        this.task.projectId = undefined;
        this.loading = false;
        window.location.reload();
      }
    );
  }

  canDeactivate(): boolean | Observable<boolean> {


    return confirm("Вы хотите покинуть страницу?");


  }
}
