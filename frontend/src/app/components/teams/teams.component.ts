import { Component, OnInit } from '@angular/core';
import { Subject, takeUntil } from 'rxjs';
import { Team } from 'src/app/models/team/team';
import { TeamService } from 'src/app/services/team.service';

@Component({
  selector: 'app-teams',
  templateUrl: './teams.component.html',
  styleUrls: ['./teams.component.css']
})
export class TeamsComponent implements OnInit {

  public teams: Team[] = [];
  public cachedTeams: Team[] = [];
  public team = {} as Team;

  private unsubscribe$ = new Subject<void>();
  public loadingTeams = false;
  public loading = false;

  constructor(
    private teamService: TeamService,
  ) { }

  ngOnInit(): void {
    this.getTeams();
  }


  public getTeams() {
    this.loadingTeams = true;
    this.teamService
      .getTeams()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (resp) => {
          this.loadingTeams = false;
          this.teams = this.cachedTeams = resp.body;
        },
        (error) => (this.loadingTeams = false)
      );
  }

  public sendTeam(){
    const teamSubscription = this.teamService.createTeam(this.team);
    this.loading = true;

    teamSubscription.pipe(takeUntil(this.unsubscribe$)).subscribe(
      (respTeam) => {
          this.addNewTeam(respTeam.body);
          this.team.name = undefined;
          this.loading = false;
      }
    );
  }

  public addNewTeam(newTeam: Team) {
    if (!this.cachedTeams.some((x) => x.id === newTeam.id)) {
        this.cachedTeams = this.sortTeamArray(this.cachedTeams.concat(newTeam));
    }    
  }

  private sortTeamArray(array: Team[]): Team[] {
    return array.sort((a, b) => +new Date(b.createdAt) - +new Date(a.createdAt));
  }
}
