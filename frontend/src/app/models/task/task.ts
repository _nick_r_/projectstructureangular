import { Project } from "../project/project";
import { User } from "../user/user";
import { State } from "../state/state";


export interface Task {
    id: number;
    name: string;
    createdAt: Date;
    finishedAt: Date;

    project: Project;
    performer: User;
    description: string;
    state: State;
}