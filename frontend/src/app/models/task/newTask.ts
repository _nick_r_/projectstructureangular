import { Project } from "../project/project";
import { State } from "../state/state";
import { User } from "../user/user";

export interface newTask {

    name: string;
    projectId: number;
    performerId: number;
    description: string;
    state: State;
}