export interface NewUser {
    firstName: string;
    lastName: string;
    email: string;
    teamId: number;
    birthDay: Date;
}