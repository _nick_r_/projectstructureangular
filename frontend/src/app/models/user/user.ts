import { TemplateLiteral } from "@angular/compiler";
import { Project } from "../project/project";
import { Task } from "../task/task";
import { Team } from "../team/team";

export interface User {
    id: number;
    registeredAt: Date;
    firstName: string;
    lastName:string;
    email: string;
    team: Team;

    tasks: Task[];
    projects: Project[];

}
