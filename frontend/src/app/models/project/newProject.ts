import { Task } from "../task/task";
import { Team } from "../team/team";
import { User } from "../user/user";

export interface NewProject {
    deadline: Date;
    authorId: number;
    teamId: number;
    name: string;
    description: string;
}