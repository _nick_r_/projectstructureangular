import { Task } from "../task/task";
import { Team } from "../team/team";
import { User } from "../user/user";

export interface Project {
    id: number;
    createdAt: Date;
    deadline: Date;

    author: User;
    team: Team;
    name: string;
    description: string;

    tasks: Task[];
}