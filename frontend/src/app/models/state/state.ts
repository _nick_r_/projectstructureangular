export enum State {
    Started,
    InProgress,
    Testing,
    Finished
}