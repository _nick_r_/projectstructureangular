import { Injectable } from '@angular/core';
import { Team } from '../models/team/team';
import { HttpInternalService } from './http-internal.service';

@Injectable({
  providedIn: 'root'
})
export class TeamService {

  public routePrefix = '/api/Teams';

  constructor(private httpService: HttpInternalService) { }

  public getTeams() {
      return this.httpService.getFullRequest<Team[]>(`${this.routePrefix}`);
  }

  public createTeam(teams: Team) {
    return this.httpService.postFullRequest<Team>(`${this.routePrefix}`, teams);
  }

    public deleteTeam(id: number) {
      return this.httpService.deleteFullRequest<Team>(`${this.routePrefix}/${id}`);
  }

  public editTeam(id : number, team: Team) {
      return this.httpService.putFullRequest<Team>(`${this.routePrefix}/${id}`, team);
  }
}
