import { Injectable } from '@angular/core';
import { HttpInternalService } from './http-internal.service';
import { Task } from 'src/app/models/task/task';
import { newTask } from '../models/task/newTask';

@Injectable({
  providedIn: 'root'
})
export class TaskService {
  
  public routePrefix = '/api/Tasks';

  constructor(private httpService: HttpInternalService) { }

  public getTasks() {
      return this.httpService.getFullRequest<Task[]>(`${this.routePrefix}`);
  }

  public createTask(teams: newTask) {
    return this.httpService.postFullRequest<Task>(`${this.routePrefix}`, teams);
  }

    public deleteTask(id: number) {
      return this.httpService.deleteFullRequest<Task>(`${this.routePrefix}/${id}`);
  }

  public editTask(id : number, task: Task) {
      return this.httpService.putFullRequest<Task>(`${this.routePrefix}/${id}`, task);
  }
}
