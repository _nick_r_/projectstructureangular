import { Injectable } from '@angular/core';
import { NewProject } from '../models/project/newProject';
import { Project } from '../models/project/project';
import { HttpInternalService } from './http-internal.service';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {

  public routePrefix = '/api/Projects';

  constructor(private httpService: HttpInternalService) { }

  public getProjects() {
      return this.httpService.getFullRequest<Project[]>(`${this.routePrefix}`);
  }

  public createProject(projects: NewProject) {
    return this.httpService.postFullRequest<Project>(`${this.routePrefix}`, projects);
  }
  
    public deleteProject(id: number) {
      return this.httpService.deleteFullRequest<Project>(`${this.routePrefix}/${id}`);
  }

  public editProject(id : number, project: Project) {
      return this.httpService.putFullRequest<Project>(`${this.routePrefix}/${id}`, project);
  }
}
