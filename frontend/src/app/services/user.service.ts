import { Injectable } from '@angular/core';
import { NewUser } from '../models/user/newUser';
import { User } from '../models/user/user';
import { HttpInternalService } from './http-internal.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

 
  public routePrefix = '/api/Users';

  constructor(private httpService: HttpInternalService) { }

  public getUsers() {
      return this.httpService.getFullRequest<User[]>(`${this.routePrefix}`);
  }

  public createUser(users: NewUser) {
    return this.httpService.postFullRequest<User>(`${this.routePrefix}`, users);
  }

    public deleteUser(id: number) {
      return this.httpService.deleteFullRequest<User>(`${this.routePrefix}/${id}`);
  }

  public editUser(id : number, user: User) {
      return this.httpService.putFullRequest<User>(`${this.routePrefix}/${id}`, user);
  }

}
