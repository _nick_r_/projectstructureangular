import { Pipe, PipeTransform } from '@angular/core';
import { stringify } from 'querystring';
  
@Pipe({
    name: 'format'
})
export class FormatPipe implements PipeTransform {
  transform(value: Date, args?: any): string {

    let date = new Date(value);
    let month = "";
    month = date.getMonth().toString();

    switch (date.getMonth()) {
        case 0:
            month = "Січня"
            break;
        case 1:
            month = "Лютого"
            break;
        case 2:
            month = "Березня"
            break;
        case 3:
            month = "Квітня"
            break;
        case 4:
            month = "Травня"
            break;
        case 5:
            month = "Червня"
            break;
        case 6:
            month = "Липня"
            break;
        case 7:
            month = "Серпня"
            break;
        case 8:
            month = "Вересня"
            break;
        case 9:
            month = "Жовтня"
            break;
        case 10:
            month = "Листопада"
            break;
        case 11:
            month = "Грудня"
            break;
    
        default:
            break;
    }
    let day = date.getDay() + 1;

    let result = day.toString() + " " + month + " " + date.getFullYear().toString();

    return result;
  }
}